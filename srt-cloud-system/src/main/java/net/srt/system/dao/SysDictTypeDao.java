package net.srt.system.dao;

import net.srt.framework.mybatis.dao.BaseDao;
import net.srt.system.entity.SysDictTypeEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 字典类型
 *
 * @author 阿沐 babamu@126.com
 */
@Mapper
public interface SysDictTypeDao extends BaseDao<SysDictTypeEntity> {

}
