package net.srt.api.module.data.governance.dto.distribute;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @ClassName LastPkJson
 * @Author zrx
 * @Date 2023/10/8 15:00
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class IncrField {
	private String name;
	private Object value;
}
