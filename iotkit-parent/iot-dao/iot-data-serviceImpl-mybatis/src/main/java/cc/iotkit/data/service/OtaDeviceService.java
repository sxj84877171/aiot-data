package cc.iotkit.data.service;

import cc.iotkit.data.model.TbOtaDevice;
import com.baomidou.mybatisplus.extension.service.IService;

public interface OtaDeviceService extends IService<TbOtaDevice> {
}
