package cc.iotkit.data.service;

import cc.iotkit.data.model.TbSysApp;
import com.baomidou.mybatisplus.extension.service.IService;

public interface SysAppService extends IService<TbSysApp> {
}
