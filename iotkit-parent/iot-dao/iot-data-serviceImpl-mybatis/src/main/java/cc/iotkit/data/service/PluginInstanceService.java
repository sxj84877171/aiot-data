package cc.iotkit.data.service;

import cc.iotkit.data.model.TbPluginInstance;
import com.baomidou.mybatisplus.extension.service.IService;

public interface PluginInstanceService extends IService<TbPluginInstance> {
}
