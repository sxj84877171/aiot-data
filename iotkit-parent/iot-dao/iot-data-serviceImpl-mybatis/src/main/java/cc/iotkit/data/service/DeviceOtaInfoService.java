package cc.iotkit.data.service;

import cc.iotkit.data.model.TbDeviceOtaInfo;
import com.baomidou.mybatisplus.extension.service.IService;

public interface DeviceOtaInfoService extends IService<TbDeviceOtaInfo> {
}
